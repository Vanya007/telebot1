#webhooks + бд
import logging
import time
from datetime import datetime
import flask
import telebot
import sqlite3
import keyboard
from sql import SQL
BD = SQL('main.db')


API_TOKEN = '6849904445:AAFhM_e9BgdXQL28zlOS-FzUBzoHF5MJGq8'

WEBHOOK_HOST = "193.169.105.93"# 195.2.93.144   94.103.82.16
WEBHOOK_PORT = "443"  # 443, 80, 88 or 8443 (port need to be 'open')
WEBHOOK_LISTEN = "0.0.0.0"  # In some VPS you may need to put here the IP addr

WEBHOOK_SSL_CERT = '/home/vladimir/telebot1/webhook_cert.pem'  # Path to the ssl certificate
WEBHOOK_SSL_PRIV = '/home/vladimir/telebot1/webhook_pkey.pem'  # Path to the ssl private key

WEBHOOK_URL_BASE = "https://%s:%s" % (WEBHOOK_HOST, WEBHOOK_PORT)
WEBHOOK_URL_PATH = "/%s/" % (API_TOKEN)

logger = telebot.logger
telebot.logger.setLevel(logging.INFO)

bot = telebot.TeleBot(API_TOKEN)

app = flask.Flask(__name__)

# Empty webserver index, return nothing, just http 200
@app.route('/', methods=['GET', 'HEAD'])
def index():
    return ''

# Process webhook calls
@app.route("/", methods=['POST'])
def webhook():
    if flask.request.headers.get('content-type') == 'application/json':
        json_string = flask.request.get_data().decode('utf-8')
        update = telebot.types.Update.de_json(json_string)
        bot.process_new_updates([update])
        return ''
    else:
        flask.abort(403)


# --- тело бота ---
online = {}
MSG_LOG = -1001466565371
MSG_LOG_NEW_USER = -1001483990739
MSG_ADM = 345632366
ocheredi = []
igroc = {}  # списки
year = {}
city = {}
f_city = {}
city1 = {"Вінницька","Волинська","Дніпропетровська","Донецька","Житомирська","Закарпатська","Запорізька"
    ,"Івано-Франківська","Київська","Кіровоградська","Луганська","Львівська","Миколаївська","Одеська","Полтавська"
    ,"Рівненська","Сумська","Тернопільська","Харківська","Херсонська","Хмельницька","Черкаська","Чернігівська","Чернівецька","",}

class igroki():
    def __init__(self,igrok1,igrok2):
        self.igrok1 = igrok1
        self.igrok2 = igrok2

@bot.message_handler(commands=["online"])
def handle_text(message):
    if message.chat.id == 345632366:
        a = BD.reklama()
        online1 = 0
        online2 = 0
        online3 = 0
        online4 = 0
        x1 = time.time() - 3600
        x2 = time.time() - 3600*4
        x3 = time.time() - 3600*12
        x4 = time.time() - 3600*24
        for i in a:
            r = int("".join(map(str, i)))
            try:
                if online[r] >= x1:
                    online1 += 1
                elif online[r] >= x2:
                    online2 += 1
                elif online[r] >= x3:
                    online3 += 1
                elif online[r] >= x4:
                    online4 += 1
                time.sleep(0.1)
            except:
                time.sleep(0.01)

        bot.send_message(MSG_ADM, "Онлайн\nПоследний час: " + str(online1) + "\nПоследние 4 часа: " + str(online2) + "\nПоследние 12 часа: " + str(online3) + "\nПоследние 24 часа: " + str(online4) + "\n\nКоличество пользователей: " + str(BD.users()))


@bot.message_handler(commands=["1"])
def handle_text(message):
    #ocheredi11 = 21
    #print(str(ocheredi))
    bot.send_message(MSG_ADM, str(ocheredi))

@bot.message_handler(commands=["answer"])
def handle_text(message):
    if message.chat.id == MSG_ADM:
        try:
            bot.send_message(message.text.split()[1], "*[REPORT]*\nВідповідь від адміністрацій:\n" + message.text.split(' ', 2)[2], parse_mode='Markdown')
        except:
            bot.send_message(message.chat.id, "!Введиде /answer [ід][текст]")

@bot.message_handler(commands=["report"])
def handle_text(message):
    if message.text == "/report" or message.text == "/report ":
        bot.send_message(message.chat.id, "!Введиде /report [текст]")  # d
    else:
        bot.send_message(MSG_ADM, "[REPORT]\n[Ід користувача] /" + str(message.chat.id) + "\n"  + message.text +  "\n/answer")

@bot.message_handler(commands=["user"])
def handle_text(message):
    if message.chat.id == 345632366:
        bot.send_message(message.chat.id, BD.users())

@bot.message_handler(commands=["bd"])
def handle_text(message):
    if message.chat.id == 345632366:
        if message.text == "/bd" or message.text == "/bd ":
            bot.send_message(message.chat.id, "Введіть /bd [id]")
        else:
            try:
                id1 = message.text.split()[1]
                bot.send_message(message.chat.id, "sex: " + str(BD.sex(id1)) + "\nyear: " + str(BD.year(id1)) + "\nf_sex: " + str(BD.f_sex(id1)) + "\nf_year: " + BD.f_year(id1))
            except:
                bot.send_message(message.chat.id, "!Введіть /bd [id]")

@bot.message_handler(commands=["cm"])
def handle_text(message):
    if message.chat.id == 345632366:
        try:
            bot.send_message(message.chat.id,bot.get_chat_member((message.text).strip("/cm "), (message.text).strip("/cm ")).user)
        except:
            bot.send_message(message.chat.id, "Введіть /cm [id]")#d


@bot.message_handler(commands=["stop"])
def handle_text(message):
    global ocheredi  #d
    if message.chat.id in igroc:
        bot.send_message(message.chat.id, "Ви успішно покинули чат")
        try:
            bot.send_message(igroc[message.chat.id].igrok1,"Ваш співрозмовник покинув чат")
            # print("id: " + str(message.chat.id) + " - /stop покинул чат")
        except:
            BD.dell_user(str(igroc[message.chat.id].igrok1))

        bot.send_message(MSG_LOG, "id: " + str(message.chat.id) + " - /stop покинув чергу")
        del igroc[igroc[message.chat.id].igrok1]
        del igroc[message.chat.id]
    elif message.chat.id in ocheredi:
        bot.send_message(message.chat.id,"Ви успішно покинули чергу")
        # print("id:" + str(message.chat.id) + " - /stop очередь")
        bot.send_message(MSG_LOG, "id: " + str(message.chat.id) + " - /stop черга")
        ocheredi.remove(message.chat.id)
    else:
        bot.send_message(message.chat.id,"-")

@bot.message_handler(commands=["v"])
def handle_text(message):
    bot.send_message(message.chat.id, "Version: 5")

@bot.message_handler(commands=["help"])
def handle_text(message):
    bot.send_message(message.chat.id,
                     "*Бот створений для анонімних переписок.* \nПри введенні команди __/start__ бот з'єднує двох рандомних користувачів "
                     "і вони спілкуються між собою анонімно\n*Доступні команди:*\n/start - починати чат\n/stop - зупинити чат\n/help - докладніше про бот"
                     "\nПомітили помилку? Щось не працює корректно? - зв'яжіться з нами і ми швидко все виправимо /report\n/report - для зв'язку з адміністрацією бота",
                     parse_mode='Markdown')


@bot.message_handler(commands=["start"])
def handle_text(message):
    online[message.chat.id] = time.time()
    global ocheredi
    if not BD.exist_user(str(message.chat.id)):
        BD.add_user(str(message.chat.id), str(datetime.now().date()))
        bot.send_message(MSG_LOG_NEW_USER,  "id: " + str(message.chat.id) + " -> новый пользователь")

    if BD.year(str(message.chat.id)) < str(16) and BD.year(str(message.chat.id)) != str(0):
        year_local1= 1
    elif BD.year(str(message.chat.id)) > str(15) and BD.year(str(message.chat.id)) < str(21):
        year_local1 = 2
    elif BD.year(str(message.chat.id)) > str(20):
        year_local1 = 3
    else:
        year_local1 = 0

    if not message.chat.id in igroc:
        if len(ocheredi) != 0:
            for l in ocheredi:
                a = ocheredi.index(l)
                b = ocheredi[a]

                if BD.year(str(b)) < str(16) and BD.year(str(b)) != str(0):
                    year_local2 = 1
                elif BD.year(str(b)) > str(15) and BD.year(str(b)) < str(21):
                    year_local2 = 2
                elif BD.year(str(b)) > str(20):
                    year_local2 = 3
                else:
                    year_local2 = 0

                sex1 = str(BD.sex(str(message.chat.id)))
                sex2 = str(BD.sex(str(b)))
                f_sex1 = str(BD.f_sex(str(message.chat.id)))
                f_sex2 = str(BD.f_sex(str(b)))
                year1 = str(year_local1)
                year2 = str(year_local2)
                f_year1 = str(BD.f_year(str(message.chat.id)))
                f_year2 = str(BD.f_year(str(b)))
                if b == message.chat.id:
                    return bot.send_message(message.chat.id, "Ви вже в чергзі, щоб покинути введіть команду /stop")
                elif (sex1, f_sex1, year1, f_year1) == (f_sex2, sex2, f_year2, year2):#идеал
                    bot.send_message(MSG_LOG, "метод 1")
                    return oceredi_all(message.chat.id, b)
                elif (sex1, f_sex1, f_year1, f_year2) == (f_sex2, sex2, str(0), str(0)):#sex1 = sex2
                    bot.send_message(MSG_LOG, "метод 2")
                    return oceredi_all(message.chat.id, b)
                elif (year1, f_year1 ,f_sex1, f_sex2) == (f_year2, year2, str(0), str(0)):#year = year
                    bot.send_message(MSG_LOG, "метод 3")
                    return oceredi_all(message.chat.id, b)
                elif (str(0), f_sex1, str(0), f_year1) == (f_sex2, sex2, f_year2, year2):#у первого 2 филтра у второго 0
                    bot.send_message(MSG_LOG, "метод 4")
                    return oceredi_all(message.chat.id, b)
                elif (sex1, f_sex1, year1, f_year1) == (f_sex2, str(0), f_year2, str(0)):#у первого 0 филтра у второго 2
                    bot.send_message(MSG_LOG, "метод 5")
                    return oceredi_all(message.chat.id, b)
                elif (str(0), f_sex1, str(0), f_year1) == (f_sex2, sex2, f_year2, str(0)):#у первого sex = sex2 а у второго = 0
                    bot.send_message(MSG_LOG, "метод 6")
                    return oceredi_all(message.chat.id, b)
                elif (sex1, f_sex1, str(0), f_year1) == (f_sex2, str(0), f_year2, str(0)):#у второго sex = sex1 а у первого = 0
                    bot.send_message(MSG_LOG, "метод 7")
                    return oceredi_all(message.chat.id, b)
                elif (str(0), f_sex1, str(0), f_year1) == (f_sex2, str(0), f_year2, year2):#у первого year = year2 а у второго = 0
                    bot.send_message(MSG_LOG, "метод 8")
                    return oceredi_all(message.chat.id, b)
                elif (str(0), f_sex1, year1, f_year1) == (f_sex2, str(0), f_year2, str(0)):#у второго year = year1 а у первого = 0
                    bot.send_message(MSG_LOG, "метод 9")
                    return oceredi_all(message.chat.id, b)
                elif (sex1, f_sex1, str(0), f_year1) == (f_sex2, sex2, f_year2, year2) or (
                        sex1, f_sex1, year1, f_year1) == (f_sex2, sex2, f_year2, str(0)):#у одного 2 филтра у другого f_year = 0
                    bot.send_message(MSG_LOG, "метод 10")
                    return oceredi_all(message.chat.id, b)
                elif (str(0), f_sex1, year1, f_year1) == (f_sex2, sex2, f_year2, year2) or (
                            sex1, f_sex1, year1, f_year1) == (f_sex2, str(0), f_year2, year2):#у одного 2 филтра у другого f_sex = 0
                    bot.send_message(MSG_LOG, "метод 11")
                    return oceredi_all(message.chat.id, b)
                elif (str(0), f_sex1, str(0), f_year1) == (f_sex2, str(0), f_year2, str(0)) and (
                            (sex1, str(0), year1, str(0)) == (str(0), sex2, str(0), year2)):#все фильтры и весь профиль по нулям f_ = 0
                    bot.send_message(MSG_LOG, "метод 12")
                    return oceredi_all(message.chat.id, b)
                elif (str(0), f_sex1, str(0), f_year1) == (f_sex2, str(0), f_year2, str(0)):#все фильтры по нулям
                    bot.send_message(MSG_LOG, "метод 13")
                    return oceredi_all(message.chat.id, b)
            else:
                bot.send_message(MSG_LOG, "метод 14\n" + str(ocheredi))
                return oceredi_all(message.chat.id, 0)
        else:
            return oceredi_all(message.chat.id, 0)
    else:
        bot.send_message(message.chat.id, "Щоб покинути чат, введіть команду /stop")


rek = False
@bot.message_handler(commands=["rek"])
def handle_text(message):
    global rek
    if message.chat.id == 345632366 and rek == False:
        rek = True
        bot.send_message(message.chat.id, "Rek = True\nведите текст для отправки:")
        return
    elif message.chat.id == 345632366 and rek == True:
        rek = False
        bot.send_message(message.chat.id, "Rek = False")
        return


@bot.message_handler(content_types=["text"])
def handle_text(message):
    global rek
    online[message.chat.id] = time.time()
    ##new------------
    if message.chat.id in city and message.text != "🔙Назат" and message.text in city1:
        BD.add_city(message.chat.id, message.text)
        del city[message.chat.id]
        return bot.send_message(message.chat.id, "Ви обрали: " + message.text, reply_markup=keyboard.filtr_search)

    if message.chat.id in f_city and message.text != "🔙Назат" and message.text in city1:
        BD.add_f_city(message.chat.id, message.text)
        del f_city[message.chat.id]
        return bot.send_message(message.chat.id, "Ви обрали: " + message.text, reply_markup=keyboard.setting_profiles)


    if message.chat.id in year:
        try:
            if int(message.text) < 100 and int(message.text) > 10:
                BD.add_year(message.chat.id, message.text)
                del year[message.chat.id]
                return bot.send_message(message.chat.id, "Ваш вік: " + message.text)
            else:
                return bot.send_message(message.chat.id, "Вкажіть, будь ласка вік цифрою від 11 до 99")
        except:
            return bot.send_message(message.chat.id, "Вкажіть, будь ласка вік цифрою від 11 до 99")


    elif message.chat.id == MSG_ADM and rek == True:
        a = BD.reklama()
        for i in a:
            r = int("".join(map(str, i)))
            try:
                bot.send_message(r, message.text)
            except:
                BD.dell_user(r)
            time.sleep(1)
        bot.send_message(MSG_ADM, "Рассылка завершена")




    elif message.text == "👤Профіль":
        a3 = ''.join(str(e) for e in BD.goal(str(message.chat.id)))
        a1 = ''.join(str(e) for e in BD.year(str(message.chat.id)))
        a2 = ''.join(str(e) for e in BD.sex(str(message.chat.id)))
        if a3 == "1":
            b = 'Просте спілкування'
        elif a3 == "2":
            b = 'Романтика та флірт'
        elif a3 == "3":
            b = 'Флірт 18+'
        else:
            b = "Не вказано"

        if a2 == "1":
            a = 'Чоловічий'
        elif a2 == "2":
            a = 'Жіночий'
        else:
            a = "Не вказано"

        if str(BD.city(str(message.chat.id)))[3:-4] == str(0):
            b1 = "Не вказано"
        else:
            b1 = str(BD.city(str(message.chat.id)))[3:-4]

        bot.send_message(message.chat.id,
                         "*Особисті дані:*\nВік: " + a1 + "\nСтать: " + a + "\nМета спілкування: " + b + "\nОбласть:" + b1,
                         reply_markup=keyboard.profiles, parse_mode='Markdown')


    elif message.text == "🔙Назат":
        bot.send_message(message.chat.id, "Ви повернулися до меню", reply_markup=keyboard.menu)

    elif message.text == "⚙Налаштувати профіль":
        bot.send_message(message.chat.id, "Вкажіть відповідні дані", reply_markup=keyboard.setting_profiles)

    elif message.text == "⚙️Стать":
        bot.send_message(message.chat.id, "Виберіть будь ласка свою стать", reply_markup=keyboard.sex)

    elif message.text == "⚙Вік":
        bot.send_message(message.chat.id, "Вкажіть будь ласка свій вік", reply_markup=keyboard.year)
        year[message.chat.id] = True

    elif message.text == "⚙️Мета спілкування":
        bot.send_message(message.chat.id, "Виберіть будь ласка відповідний варіант", reply_markup=keyboard.goal)
    elif message.text == "⚙️Область":
        bot.send_message(message.chat.id, "Виберіть будь ласка відповідний варіант", reply_markup=keyboard.city)
        city[message.chat.id] = True
    elif message.text == "🌍Користувачі":
        bot.send_message(message.chat.id, "В розробці")
    elif message.text == "🎭Ігри":
        bot.send_message(message.chat.id, "В розробці")

    elif message.text == "🔍Фільтр пошуку":
        if str(BD.sex(str(message.chat.id))) == str(0) or str(BD.year(str(message.chat.id))) == str(0):
            return bot.send_message(message.chat.id, "Для того щоб ви змогли налаштувати фільтр пошуку, потрібно вказати дані про себе\nПрофіль -> ⚙️Налаштувати профіль", reply_markup=keyboard.setting_profiles)
        a1 = ''.join(str(e) for e in BD.f_year(str(message.chat.id)))
        a2 = ''.join(str(e) for e in BD.f_sex(str(message.chat.id)))
        a3 = ''.join(str(e) for e in BD.f_goal(str(message.chat.id)))

        if a1 == str(1):
            b = 'до 15'
        elif a1 == str(2):
            b = 'от 16 до 20'
        elif a1 == str(3):
            b = '21+'
        else:
            b = 'Не вказано'

        if a2 == str(1):
            a = 'Чоловічий'
        elif a2 == str(2):
            a = 'Жіночий'
        else:
            a = 'Не вказано'

        if str(BD.f_city(str(message.chat.id)))[3:-4] == str(0):
            b1 = "Не вказано"
        else:
            b1 = str(BD.f_city(str(message.chat.id)))[3:-4]


        bot.send_message(message.chat.id,
                         "*Ваші фільтри:*\nВік: " + b + "\nСтать: " + a + "\nМета спілкування: " + a3 + "\nОбласть: " + b1,
                         reply_markup=keyboard.filtr_search, parse_mode='Markdown')


    elif message.text == "⚙️ Стать.":
        bot.send_message(message.chat.id, "Оберіть будь ласка стать", reply_markup=keyboard.f_sex)

    elif message.text == "⚙️ Вік.":
        bot.send_message(message.chat.id, "Вкажіть будь ласка вік", reply_markup=keyboard.f_year)


    elif message.text == "⚙️ Мета спілкування.":
        bot.send_message(message.chat.id, "В разработке")
        #bot.send_message(message.chat.id, "Выберите пожалуйста соотвествующий вариант", reply_markup=keyboard.f_goal)
    elif message.text == "⚙️ Область.":
        bot.send_message(message.chat.id, "В разработке...")
        #bot.send_message(message.chat.id, "Выберите пожалуйста соотвествующий вариант", reply_markup=keyboard.city)
        #f_city[message.chat.id] = True
    elif message.text == "⚙️ Обнулити фільтри.":
        bot.send_message(message.chat.id, "Фільтр пошуку сброшен")
        BD.add_f_sex(message.chat.id, 0)
        BD.add_f_year(message.chat.id, 0)

    ##------------
    elif not message.chat.id in igroc:
        bot.send_message(message.chat.id,"Введіть команду /start і ми знайдемо співрозмовника\nВведіть команду /help щоб дізнатися інформацію про бот")
    elif igroc[message.chat.id].igrok1 == igroc[message.chat.id].igrok1:
        try:
            bot.send_message(igroc[message.chat.id].igrok1,message.text)
            #bot.reply_to(message,message.text)
        except:
            bot.send_message(message.chat.id, "Ваш співрозмовник залишив чат\nВведіть команду /start і ми знайдемо вам нового співрозмовника")
            del igroc[igroc[message.chat.id].igrok1]
            del igroc[message.chat.id]

@bot.message_handler(content_types=["photo"])
def handle_photo(message):
    if not message.chat.id in igroc:
        bot.send_message(message.chat.id,"Введіть команду /start і ми знайдемо співрозмовника\nВведіть команду /help щоб дізнатися інформацію про наш бот")
    elif igroc[message.chat.id].igrok1 == igroc[message.chat.id].igrok1:
        file = bot.get_file(message.photo[len(message.photo)-1].file_id)
        bot.send_photo(igroc[message.chat.id].igrok1, file.file_id)


@bot.message_handler(content_types=["audio"])
def handle_audio(message):
        if not message.chat.id in igroc:
            bot.send_message(message.chat.id,"Введіть команду /start і ми знайдемо співрозмовника\nВведіть команду /help щоб дізнатися інформацію про наш бот")
        elif igroc[message.chat.id].igrok1 == igroc[message.chat.id].igrok1:
            file = bot.get_file(message.audio.file_id)
            bot.send_audio(igroc[message.chat.id].igrok1, file.file_id)

@bot.message_handler(content_types=["video"])
def handle_video(message):
        if not message.chat.id in igroc:
            bot.send_message(message.chat.id,"Введіть команду /start і ми знайдемо співрозмовника\nВведіть команду /help щоб дізнатися інформацію про наш бот")
        elif igroc[message.chat.id].igrok1 == igroc[message.chat.id].igrok1:
            file = bot.get_file(message.video.file_id)
            bot.send_video(igroc[message.chat.id].igrok1, file.file_id)

@bot.message_handler(content_types=["voice"])
def handle_voice(message):
        if not message.chat.id in igroc:
            bot.send_message(message.chat.id,"Введіть команду /start і ми знайдемо співрозмовника\nВведіть команду /help щоб дізнатися інформацію про наш бот")
        elif igroc[message.chat.id].igrok1 == igroc[message.chat.id].igrok1:
            file = bot.get_file(message.voice.file_id)
            bot.send_voice(igroc[message.chat.id].igrok1, file.file_id)

@bot.message_handler(content_types=["sticker"])
def handle_sticker(message):
        if not message.chat.id in igroc:
            bot.send_message(message.chat.id,"Введіть команду /start і ми знайдемо співрозмовника\nВведіть команду /help щоб дізнатися інформацію про наш бот")
        elif igroc[message.chat.id].igrok1 == igroc[message.chat.id].igrok1:
            file = bot.get_file(message.sticker.file_id)
            bot.send_sticker(igroc[message.chat.id].igrok1, file.file_id)

@bot.message_handler(content_types=["document"])
def handle_document(message):
        if not message.chat.id in igroc:
            bot.send_message(message.chat.id,"Введіть команду /start і ми знайдемо співрозмовника\nВведіть команду /help щоб дізнатися інформацію про наш бот")
        elif igroc[message.chat.id].igrok1 == igroc[message.chat.id].igrok1:
            file = bot.get_file(message.document.file_id)
            bot.send_document(igroc[message.chat.id].igrok1, file.file_id)

@bot.message_handler(content_types=["contact"])
def handle_contact(message):
        if not message.chat.id in igroc:
            bot.send_message(message.chat.id,"Введіть команду /start і ми знайдемо співрозмовника\nВведіть команду /help щоб дізнатися інформацію про наш бот")
        elif igroc[message.chat.id].igrok1 == igroc[message.chat.id].igrok1:
            bot.send_contact(igroc[message.chat.id].igrok1, message.contact.phone_number,message.contact.first_name)

@bot.message_handler(content_types=["location"])
def handle_location(message):
        if not message.chat.id in igroc:
            bot.send_message(message.chat.id,"Введіть команду /start і ми знайдемо співрозмовника\nВведіть команду /help щоб дізнатися інформацію про наш бот")
        elif igroc[message.chat.id].igrok1 == igroc[message.chat.id].igrok1:
            bot.send_location(igroc[message.chat.id].igrok1, message.location.latitude, message.location.longitude)

@bot.message_handler(content_types=["chat_action"])
def handle_chat_action(message):
        if not message.chat.id in igroc:
            bot.send_message(message.chat.id,"Введіть команду /start і ми знайдемо співрозмовника\nВведіть команду /help щоб дізнатися інформацію про наш бот")
        elif igroc[message.chat.id].igrok1 == igroc[message.chat.id].igrok1:
            bot.send_chat_action(igroc[message.chat.id].igrok1, message.action)

@bot.message_handler(content_types=["video_note"])
def handle_video_note(message):
        if not message.chat.id in igroc:
            bot.send_message(message.chat.id,"Введіть команду /start і ми знайдемо співрозмовника\nВведіть команду /help щоб дізнатися інформацію про наш бот")
        elif igroc[message.chat.id].igrok1 == igroc[message.chat.id].igrok1:
            bot.send_video_note(igroc[message.chat.id].igrok1,message.video_note.file_id, message.video_note.duration, message.video_note.length,)
            #print(message)

@bot.callback_query_handler(func=lambda call: True)
def process_callback_button(call):
    if call.data == "sex1":
        bot.send_message(call.message.chat.id, 'Чоловічий')
        BD.add_sex(call.message.chat.id, 1)
    if call.data == "sex2":
        bot.send_message(call.message.chat.id, 'Жіночий')
        BD.add_sex(call.message.chat.id, 2)
    if call.data == "year":
        bot.send_message(call.message.chat.id, "Ви повернулися до меню налаштувань", reply_markup=keyboard.setting_profiles)
        del year[call.message.chat.id]
    if call.data == "goal1":
        bot.send_message(call.message.chat.id, 'Ви обрали: Просте спілкування')
        BD.add_goal(call.message.chat.id, 1)
    if call.data == "goal2":
        bot.send_message(call.message.chat.id, 'Ви обрали: Романтика та флірт')
        BD.add_goal(call.message.chat.id, 2)
    if call.data == "goal3":
        bot.send_message(call.message.chat.id, 'Ви обрали: Флірт 18+')
        BD.add_goal(call.message.chat.id, 3)
    ##фильтры
    if call.data == "f_sex1":
        bot.send_message(call.message.chat.id, 'Чоловічий')
        BD.add_f_sex(call.message.chat.id, 1)
    if call.data == "f_sex2":
        bot.send_message(call.message.chat.id, 'Жіночий')
        BD.add_f_sex(call.message.chat.id, 2)
    if call.data == "f_goal1":
        bot.send_message(call.message.chat.id, 'Ви обрали: Просте спілкування')
        BD.add_f_goal(call.message.chat.id, 1)
    if call.data == "f_goal2":
        bot.send_message(call.message.chat.id, 'Ви обрали: Романтика та флірт')
        BD.add_f_goal(call.message.chat.id, 2)
    if call.data == "f_goal3":
        bot.send_message(call.message.chat.id, 'Ви обрали: Флірт 18+')
        BD.add_f_goal(call.message.chat.id, 3)

    if call.data == "f_year1":
        bot.send_message(call.message.chat.id, 'Ви обрали: До 15')
        BD.add_f_year(call.message.chat.id, 1)
    if call.data == "f_year2":
        bot.send_message(call.message.chat.id, 'Ви обрали: От 16 до 20')
        BD.add_f_year(call.message.chat.id, 2)
    if call.data == "f_year3":
        bot.send_message(call.message.chat.id, 'Ви обрали: 21+')
        BD.add_f_year(call.message.chat.id, 3)

def oceredi_all(user_id, o_oceredi):
    global ocheredi
    if str(BD.f_sex(user_id)) == str(1):
        a = 'Чоловічий'
    elif str(BD.f_sex(user_id)) == str(2):
        a = 'Жіночий'
    else:
        a = 'Не вказано'

    if str(BD.f_year(user_id)) == str(1):
        b = 'до 15'
    elif str(BD.f_year(user_id)) == str(2):
        b = 'от 16 до 20'
    elif str(BD.f_year(user_id)) == str(3):
        b = '21+'
    else:
        b = 'Не вказано'
    if o_oceredi == 0 and not user_id in igroc and not user_id in ocheredi:
        bot.send_message(user_id,
                         "Ви успішно встали в чергу, зачекайте трохи, поки ми знайдемо вам співрозмовника\n*Ваші параметри для пошуку:*\nСтать: " + a + "\nВік: " + b + "\n\nЩоб покинути чергу введіть команду /stop",
                         parse_mode='Markdown', reply_markup=keyboard.menu)
        # print("id: " + str(message.chat.id) + " - /start очередь")
        bot.send_message(MSG_LOG, "id: " + str(user_id) + " - /start очередь")
        ocheredi.append(user_id)
    elif user_id in ocheredi:
        return bot.send_message(user_id, "Ви вже в черзі, щоб покинути введіть команду /stop", reply_markup=keyboard.menu)
    else:
        try:
            bot.send_message(o_oceredi, "Ви з'єднані, можете спілкуватись\nЩоб покинути чат введіть команду /stop")
            bot.send_message(user_id,
                             "Ви поєднані, можете спілкуватися\nЩоб покинути чат введіть команду /stop", reply_markup=keyboard.menu)
            igroc[user_id] = igroki(o_oceredi, user_id)
            igroc[o_oceredi] = igroki(user_id, o_oceredi)
            bot.send_message(MSG_LOG,"id: " + str(user_id) + " and id: " + str(int(o_oceredi)) + " - соедены")
            ocheredi.remove(o_oceredi)
        except:
            ocheredi.remove(o_oceredi)
            BD.dell_user(o_oceredi)
            ocheredi.append(user_id)
            bot.send_message(user_id,
                             "Ви успішно встали в чергу, зачекайте, поки ми знайдемо вам співрозмовника\n*Ваші параметри для пошуку:*\nСтать: " + a + "\nВік: " + b + "\n\nЩоб покинути чергу введіть команду /stop",
                             parse_mode='Markdown', reply_markup=keyboard.menu)
            bot.send_message(MSG_LOG, "id: " + str(user_id) + " - /start очередь")

# --- тело бота ---

# Remove webhook, it fails sometimes the set if there is a previous webhook
bot.remove_webhook()

try:
    time.sleep(0.5)
    bot.set_webhook(url="https://193.169.105.93:443", certificate=open(WEBHOOK_SSL_CERT, 'r'))
except:
    time.sleep(2)
    bot.set_webhook(url="https://193.169.105.93:443", certificate=open(WEBHOOK_SSL_CERT, 'r'))

# Set webhook
#bot.set_webhook(url="https://94.103.82.16:88", certificate=open(WEBHOOK_SSL_CERT, 'r'))

# Start flask server
app.run(host=WEBHOOK_HOST,
        port=WEBHOOK_PORT,
        ssl_context=(WEBHOOK_SSL_CERT, WEBHOOK_SSL_PRIV),
        debug=True)